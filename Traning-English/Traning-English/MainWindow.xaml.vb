﻿Imports System.Speech.Recognition
Imports System.Speech.Synthesis
Imports System.Windows.Media.Animation
Class MainWindow
    Private SpeechRecEngineJp As New SpeechRecognitionEngine(New System.Globalization.CultureInfo("ja-JP"))
    Private SpeechRecEngine As New SpeechRecognitionEngine(New System.Globalization.CultureInfo("en-US"))
    Private SpeechSynth As New SpeechSynthesizer
    Private GrammersList As System.Collections.ObjectModel.ReadOnlyCollection(Of InstalledVoice)
    Private IsRecodingJp As Boolean = False
    Private IsRecoding As Boolean = False
    Private IsSpeeching As Boolean = False

    Private Sub Window_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        TalkBox.Text = "Now Loading"
        RecStartButton.IsEnabled = False
        SpeechStartButton.IsEnabled = False

        Task.Run(Sub()
                     SpeechRecEngine.SetInputToDefaultAudioDevice()
                     SpeechRecEngineJp.SetInputToDefaultAudioDevice()
                     GrammersList = SpeechSynth.GetInstalledVoices()
                     SpeechRecEngine.LoadGrammar(New DictationGrammar())
                     SpeechRecEngineJp.LoadGrammar(New DictationGrammar())
                     AddHandler SpeechRecEngine.SpeechRecognized, AddressOf Speech_Rec_Ended
                     AddHandler SpeechRecEngineJp.SpeechRecognized, AddressOf Speech_Rec_Ended
                     AddHandler SpeechSynth.SpeakCompleted, Sub()
                                                                RecStartButton.IsEnabled = True
                                                                IsSpeeching = False
                                                            End Sub
                     Dispatcher.BeginInvoke(Sub()
                                                GrammersComboBox.ItemsSource = GrammersList.Select(Function(s) s.VoiceInfo.Name + "(" + s.VoiceInfo.Culture.DisplayName + ")")
                                                GrammersComboBox.SelectedIndex = 0
                                                TalkBox.Text = ""
                                                RecStartButton.IsEnabled = True
                                                SpeechStartButton.IsEnabled = True
                                            End Sub)
                 End Sub)
    End Sub

    Private Sub Caption_Mini(sender As Object, e As RoutedEventArgs)
        WindowState = WindowState.Minimized
    End Sub
    Private Sub Caption_Close(sender As Object, e As RoutedEventArgs)
        Close()
    End Sub
    Private Sub Caption_MN(sender As Object, e As RoutedEventArgs) Handles Caption_MaxNormal.Click
        WindowState = IIf(WindowState = WindowState.Normal, WindowState.Maximized, WindowState.Normal)
    End Sub
    Private Sub Window_StateChanged(sender As Object, e As EventArgs) Handles Me.StateChanged
        If Me.WindowState = System.Windows.WindowState.Maximized Then
            Caption_MaxNormal.Content = "2"
        ElseIf Me.WindowState = System.Windows.WindowState.Minimized Then
            Caption_MaxNormal.Content = "1"
        End If
    End Sub

    Private Sub Speech_Rec_Ended(sender As Object, e As SpeechRecognizedEventArgs)
        Dispatcher.BeginInvoke(Sub()
                                   TalkBox.Text += e.Result.Text & Environment.NewLine
                                   SpeechStartButton.IsEnabled = True
                                   IsRecoding = IsRecodingJp = False
                                   StateEllipse.Stroke = New SolidColorBrush(Colors.White)
                                   LangChangeBox.IsEnabled = True
                               End Sub)
    End Sub
    Private Sub Rec_Start(sender As Object, e As RoutedEventArgs) Handles RecStartButton.Click
        If LangChangeBox.SelectedIndex = 0 Then
            If IsRecoding Then
                SpeechRecEngine.RecognizeAsyncCancel()
                SpeechStartButton.IsEnabled = True
                IsRecoding = False
                StateEllipse.Stroke = New SolidColorBrush(Colors.White)
                LangChangeBox.IsEnabled = True
            Else
                SpeechRecEngine.RecognizeAsync()
                SpeechStartButton.IsEnabled = False
                IsRecoding = True
                StateEllipse.Stroke = New SolidColorBrush(Colors.Red)
                LangChangeBox.IsEnabled = False
            End If
        Else
            If IsRecodingJp Then
                SpeechRecEngineJp.RecognizeAsyncCancel()
                SpeechStartButton.IsEnabled = True
                IsRecodingJp = False
                StateEllipse.Stroke = New SolidColorBrush(Colors.White)
                LangChangeBox.IsEnabled = True
            Else
                SpeechRecEngineJp.RecognizeAsync()
                SpeechStartButton.IsEnabled = False
                IsRecodingJp = True
                StateEllipse.Stroke = New SolidColorBrush(Colors.Red)
                LangChangeBox.IsEnabled = False
            End If
        End If
    End Sub

    Private Sub Speech_Start(sender As Object, e As RoutedEventArgs) Handles SpeechStartButton.Click
        If IsSpeeching Then
            If SpeechSynth.State = SynthesizerState.Speaking Then
                SpeechSynth.SpeakAsyncCancelAll()
            End If
            RecStartButton.IsEnabled = True
            IsSpeeching = False
            StateEllipse.Stroke = New SolidColorBrush(Colors.White)
        Else
            SpeechSynth.SelectVoice(GrammersList(GrammersComboBox.SelectedIndex).VoiceInfo.Name)
            SpeechSynth.SpeakAsync(TalkBox.Text)
            RecStartButton.IsEnabled = False
            IsSpeeching = True
            StateEllipse.Stroke = New SolidColorBrush(Colors.AliceBlue)
        End If
    End Sub
End Class
