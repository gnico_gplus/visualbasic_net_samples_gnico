﻿Class MainWindow
    Private count As Byte = 0
    Private Sub AddName(sender As Object, e As RoutedEventArgs) Handles Add.Click
        If NameBox.Text <> "" Then
            count += 1
        End If
        If count < 26 Then
            Dim rand As New Random()
            Dim c As Byte
            Dim Name As String = NameBox.Text
            NameBox.Text = ""
            While True
                c = rand.Next(24)
                Select Case c
                    Case 0
                        If _1.Text = "" Then
                            _1.Text = Name
                            GoTo e
                        End If
                    Case 1
                        If _2.Text = "" Then
                            _2.Text = Name
                            GoTo e
                        End If
                    Case 2
                        If _3.Text = "" Then
                            _3.Text = Name
                            GoTo e
                        End If
                    Case 3
                        If _4.Text = "" Then
                            _4.Text = Name
                            GoTo e
                        End If
                    Case 4
                        If _5.Text = "" Then
                            _5.Text = Name
                            GoTo e
                        End If
                    Case 5
                        If _6.Text = "" Then
                            _6.Text = Name
                            GoTo e
                        End If
                    Case 6
                        If _7.Text = "" Then
                            _7.Text = Name
                            GoTo e
                        End If
                    Case 7
                        If _8.Text = "" Then
                            _8.Text = Name
                            GoTo e
                        End If
                    Case 8
                        If _9.Text = "" Then
                            _9.Text = Name
                            GoTo e
                        End If
                    Case 9
                        If _10.Text = "" Then
                            _10.Text = Name
                            GoTo e
                        End If
                    Case 10
                        If _11.Text = "" Then
                            _11.Text = Name
                            GoTo e
                        End If
                    Case 11
                        If _12.Text = "" Then
                            _12.Text = Name
                            GoTo e
                        End If
                    Case 12
                        If _13.Text = "" Then
                            _13.Text = Name
                            GoTo e
                        End If
                    Case 13
                        If _14.Text = "" Then
                            _14.Text = Name
                            GoTo e
                        End If
                    Case 14
                        If _15.Text = "" Then
                            _15.Text = Name
                            GoTo e
                        End If
                    Case 15
                        If _16.Text = "" Then
                            _16.Text = Name
                            GoTo e
                        End If
                    Case 16
                        If _17.Text = "" Then
                            _17.Text = Name
                            GoTo e
                        End If
                    Case 17
                        If _18.Text = "" Then
                            _18.Text = Name
                            GoTo e
                        End If
                    Case 18
                        If _19.Text = "" Then
                            _19.Text = Name
                            GoTo e
                        End If
                    Case 19
                        If _20.Text = "" Then
                            _20.Text = Name
                            GoTo e
                        End If
                    Case 20
                        If _21.Text = "" Then
                            _21.Text = Name
                            GoTo e
                        End If
                    Case 21
                        If _22.Text = "" Then
                            _22.Text = Name
                            GoTo e
                        End If
                    Case 22
                        If _23.Text = "" Then
                            _23.Text = Name
                            GoTo e
                        End If
                    Case 23
                        If _24.Text = "" Then
                            _24.Text = Name
                            GoTo e
                        End If
                End Select
            End While
e:
        End If
    End Sub

    Private Sub TextEntered(sender As Object, e As KeyEventArgs) Handles NameBox.KeyUp
        If e.Key = Key.Enter Then
            AddName(Nothing, Nothing)
        End If
    End Sub

    Private Sub Clean(sender As Object, e As RoutedEventArgs) Handles Clear.Click
        _1.Text = ""
        _2.Text = ""
        _3.Text = ""
        _4.Text = ""
        _5.Text = ""
        _6.Text = ""
        _7.Text = ""
        _8.Text = ""
        _9.Text = ""
        _10.Text = ""
        _11.Text = ""
        _12.Text = ""
        _13.Text = ""
        _14.Text = ""
        _15.Text = ""
        _16.Text = ""
        _17.Text = ""
        _18.Text = ""
        _19.Text = ""
        _20.Text = ""
        _21.Text = ""
        _22.Text = ""
        _23.Text = ""
        _24.Text = ""
        count = 0
    End Sub

    Private Sub SortSub(sender As Object, e As RoutedEventArgs) Handles Sort.Click
        For i As Byte = 0 To 22
            Select Case i
                Case 0
                    If _1.Text = "" Then
                        _1.Text = _2.Text
                    End If
                Case 1
                    If _2.Text = "" Then
                        _2.Text = _3.Text
                    End If
                Case 2
                    If _3.Text = "" Then
                        _3.Text = _4.Text
                    End If
                Case 3
                    If _4.Text = "" Then
                        _4.Text = _5.Text
                    End If
                Case 4
                    If _5.Text = "" Then
                        _5.Text = _6.Text
                    End If
                Case 5
                    If _6.Text = "" Then
                        _6.Text = _7.Text
                    End If
                Case 6
                    If _7.Text = "" Then
                        _7.Text = _8.Text
                    End If
                Case 7
                    If _8.Text = "" Then
                        _8.Text = _9.Text
                    End If
                Case 8
                    If _9.Text = "" Then
                        _9.Text = _10.Text
                    End If
                Case 9
                    If _10.Text = "" Then
                        _10.Text = _11.Text
                    End If
                Case 10
                    If _11.Text = "" Then
                        _11.Text = _12.Text
                    End If
                Case 11
                    If _12.Text = "" Then
                        _12.Text = _13.Text
                    End If
                Case 12
                    If _13.Text = "" Then
                        _13.Text = _14.Text
                    End If
                Case 13
                    If _14.Text = "" Then
                        _14.Text = _15.Text
                    End If
                Case 14
                    If _15.Text = "" Then
                        _15.Text = _16.Text
                    End If
                Case 15
                    If _16.Text = "" Then
                        _16.Text = _17.Text
                    End If
                Case 16
                    If _17.Text = "" Then
                        _17.Text = _18.Text
                    End If
                Case 17
                    If _18.Text = "" Then
                        _18.Text = _19.Text
                    End If
                Case 18
                    If _19.Text = "" Then
                        _19.Text = _20.Text
                    End If
                Case 19
                    If _20.Text = "" Then
                        _20.Text = _21.Text
                    End If
                Case 20
                    If _21.Text = "" Then
                        _21.Text = _22.Text
                    End If
                Case 21
                    If _22.Text = "" Then
                        _22.Text = _23.Text
                    End If
                Case 22
                    If _23.Text = "" Then
                        _23.Text = _24.Text
                    End If
            End Select
        Next
e:

    End Sub
End Class
