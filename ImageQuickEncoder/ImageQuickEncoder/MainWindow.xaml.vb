﻿Class MainWindow

    Private Sub PreviewDragOvers(sender As Object, e As DragEventArgs) Handles jpgBlock.PreviewDragOver, pngBlock.PreviewDragOver, bmpBlock.PreviewDragOver, gifBlock.PreviewDragOver, tiffBlock.PreviewDragOver
        e.Effects = If(e.Data.GetDataPresent(DataFormats.FileDrop, True), DragDropEffects.Copy, DragDropEffects.None)
        e.Handled = True
    End Sub

    Private Sub jpg_Drop(sender As Object, e As DragEventArgs) Handles jpgBlock.Drop
        Encode(ImageFormat.Jpeg, "jpg", e.Data.GetData(DataFormats.FileDrop))
    End Sub
    Private Sub png_Drop(sender As Object, e As DragEventArgs) Handles pngBlock.Drop
        Encode(ImageFormat.Png, "png", e.Data.GetData(DataFormats.FileDrop))
    End Sub
    Private Sub bmp_Drop(sender As Object, e As DragEventArgs) Handles bmpBlock.Drop
        Encode(ImageFormat.Bmp, "bmp", e.Data.GetData(DataFormats.FileDrop))
    End Sub
    Private Sub gif_Drop(sender As Object, e As DragEventArgs) Handles gifBlock.Drop
        Encode(ImageFormat.Gif, "gif", e.Data.GetData(DataFormats.FileDrop))
    End Sub
    Private Sub tiff_Drop(sender As Object, e As DragEventArgs) Handles tiffBlock.Drop
        Encode(ImageFormat.Tiff, "tiff", e.Data.GetData(DataFormats.FileDrop))
    End Sub

    Private Sub Encode(x As ImageFormat, y As String, z As String())
        Try
            For Each s As String In z
                Dim b As New Bitmap(s)
                b.Save(s.Substring(0, s.LastIndexOf(".") + 1) + y, x)
                b.Dispose()
            Next
        Catch
            MsgBox("正常に変換できませんでした", "ERROR", MsgBoxStyle.Exclamation)
        End Try
    End Sub

End Class
