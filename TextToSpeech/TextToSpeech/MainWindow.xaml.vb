﻿Imports System.Collections.ObjectModel
Imports System.Speech.Synthesis

Class MainWindow
    Private Synth As New SpeechSynthesizer()
    Dim SynthList As ReadOnlyCollection(Of InstalledVoice)

    Private Sub Drag_Window(sender As Object, e As MouseButtonEventArgs) Handles Me.MouseLeftButtonDown
        Me.DragMove()
    End Sub
    Private Sub Close_Window(sender As Object, e As RoutedEventArgs) Handles Close_Btn.Click
        Me.Close()
    End Sub
    Private Sub Mini_Window(sender As Object, e As RoutedEventArgs) Handles Mini_Btn.Click
        Me.WindowState = Windows.WindowState.Minimized
    End Sub
    Private Sub StateChange_Window(sender As Object, e As RoutedEventArgs) Handles Style_Change_Btn.Click
        Me.WindowState = If(Style_Change_Btn.Content = "1", WindowState.Maximized, WindowState.Normal)
        Style_Change_Btn.Content = If(Style_Change_Btn.Content = "1", "2", "1")
    End Sub
    Private Sub Closed_Window(sender As Object, e As EventArgs) Handles Me.Closed
        Synth.SpeakAsyncCancelAll()
        Environment.Exit(Environment.ExitCode)
    End Sub
    Private Sub Loaded_Window(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        Synth.SetOutputToDefaultAudioDevice()
        SynthList = Synth.GetInstalledVoices()
        Narator_Box.ItemsSource = SynthList.Select(Function(i As InstalledVoice)
                                                       Return String.Format("{0} ({1},{2},{3}性)", i.VoiceInfo.Name, i.VoiceInfo.Culture.DisplayName, SwitchAge(i.VoiceInfo.Age), SwitchGender(i.VoiceInfo.Gender))
                                                   End Function)
        Narator_Box.SelectedIndex = 0
    End Sub
    Private Function SwitchAge(x As VoiceAge) As String
        Select Case x
            Case VoiceAge.Adult
                Return "大人"
            Case VoiceAge.Child
                Return "子供"
            Case VoiceAge.Senior
                Return "年配"
            Case VoiceAge.Teen
                Return "10代"
            Case Else
                Return "未設定"
        End Select
    End Function
    Private Function SwitchGender(x As VoiceGender) As String
        Select Case x
            Case VoiceGender.Female
                Return "女"
            Case VoiceGender.Male
                Return "男"
            Case VoiceGender.Neutral
                Return "中"
            Case Else
                Return "未設定"
        End Select
    End Function
    Private Sub Speech(sender As Object, e As RoutedEventArgs) Handles SpeechButton.Click
        Synth.SpeakAsyncCancelAll()
        Synth.SelectVoice(SynthList(Narator_Box.SelectedIndex).VoiceInfo.Name)
        Synth.Volume = Vol_Slider.Value
        Synth.Rate = Speed_Slider.Value
        Synth.SpeakAsync(SpeechText.Text)
    End Sub
End Class
